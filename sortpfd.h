static void swap_bytes(void *a, void *b, int len) {
  char  *x = a, *y = b, z;
  while (len--) {
    z = *x; *x = *y; *y = z;
    ++x; ++y;
  }
}

static int sortpfd() {
  int l=0, r=npfd-1;
 again:
  while (l <= r && pfd[l].fd >= 0) ++l;
  while (l <= r && pfd[r].fd  < 0) --r;

  if (l < r) {
    swap_bytes(pfd+l, pfd+r, sizeof(pollfd_));
    swap_bytes(tty+l, tty+r, sizeof(tty_name_));
    goto again;
  }
  return l;
}
