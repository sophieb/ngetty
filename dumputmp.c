#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

#define S(a,b)		str_copy(a,b)
#define SN(a,b)		str_copynz(a,b,sizeof(b))
#define N(a,b,c,d)	fmt_nmb_(a,b,c,d)

int main(int argc, char **argv) {
  struct tm *tm;
  time_t tmp_time;
  char buf[1024];
  char *p;
#ifdef USE_LIBC_UTMP
  struct utmp_type *ut;
  if (argc>1) f_utmpname(argv[1]);
  f_setutent();
#else
  struct utmp_type ut[1];
  int fd=0;
  if (argc>1) {
    fd = open(argv[1], O_RDONLY);
    if (fd == -1) { write(1,buf,S(buf,"error open()\n")); _exit(1); }
  }
#endif

  write(1,buf,S(buf,"type  pid     id    user line host ip date "
		"[term exit]\n"));

  while (f_getutent()) {
    tmp_time = ut->ut_tv.tv_sec;
    tmp_time += get_tz(tmp_time);
    tm=nv_gmtime(&tmp_time);

    p = buf;
    *p++ = '[';
    p += N(p, ut->ut_type, 1,0);		p +=S(p,"] [");
    p += N(p, ut->ut_pid,  5,0);		p +=S(p,"] [");
    p += fmt_str_(p,ut->ut_id,4);	p +=S(p,"] [");
    p += SN(p, ut->ut_user);		p +=S(p,"] [");
    p += SN(p, ut->ut_line);		p +=S(p,"] [");
    p += SN(p, ut->ut_host);		p +=S(p,"] [");
    p += fmt_utmp_ip(p, (char *)ut->ut_addr_v6);	p +=S(p,"] [");

    p += fmt_ulong(p, (1900 +tm->tm_year) % 100);   *p++ = '-';
    p += N(p, 1+tm->tm_mon,2,1);      *p++ = '-';
    p += N(p, tm->tm_mday, 2,1);      *p++ = ' ';
    p += N(p, tm->tm_hour, 2,1);      *p++ = ':';
    p += N(p, tm->tm_min,  2,1);      *p++ = ':';
    p += N(p, tm->tm_sec,  2,1);      

    if (argc>2) {
      p +=S(p,"] [");
      p += fmt_ulong(p,ut->ut_exit.e_termination); *p++ = ' ';
      p += fmt_ulong(p,ut->ut_exit.e_exit);
    }

    p +=S(p,"]\n");
    write(1,buf, p-buf);
  }
  f_endutent();
  return 0;
}
