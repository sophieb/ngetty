unsigned int fmt_nmb_(char *b, unsigned long u, char len, char fill) /*EXTRACT_INCL*/ {
  unsigned char k=len;
  char ch = (fill)?'0':' ';
  do {
    if (k == 0) break;
    b[--k] = '0' + u%10;
    u /= 10;
  } while (u);
  while (k) 
    b[--k] = ch;
  return len;
} 
