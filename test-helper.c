#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/ioctl.h>
/* 
   A) $0 ./ngetty-helper /dev/ttyX [login] 
   B) $0 "-H" /dev/ttyX
   C) $0 "-O" /dev/ttyX
*/

int main(int argc , char **argv, char **env) {
  int k;
  char *x;
  if (argc<3) _exit(100);
  ioctl(0, TIOCNOTTY, 0);
  for (k=0; k<6; k++) close(k);
  if (0 != open(argv[2], O_RDWR | O_NOCTTY)) _exit(111);

  x = argv[1];
  if (x[0] != '-') {
    argv++;
    execve(*argv, argv, env);
    _exit(127);
  }

  ++x;
  switch (*x) {
  case 'O':
    if (fork()) _exit(0);
  case 'H':
    setsid();
    ioctl(0, TIOCSCTTY, 0);
    sleep(15);
    vhangup();
  }
  _exit(0);
}
