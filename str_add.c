unsigned int str_add(char *d, const char *s, unsigned int u) /*EXTRACT_INCL*/ {
  unsigned int l=0;
  for (l=0; s[l] && l<u; l++) d[l] = s[l];
  return(l);
}
