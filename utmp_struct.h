#ifndef UTMP_STRUCT_H
#define UTMP_STRUCT_H
#include "lib.h"

#if defined(HAVE_C_UTMP) && defined(HAVE_C_UTMPX)
#error	define olny one of HAVE_C_UTMP, HAVE_C_UTMPX in lib.h
#endif

#undef USE_LIBC_UTMP
#if defined(HAVE_C_UTMP) || defined(HAVE_C_UTMPX)
#define USE_LIBC_UTMP
#endif

#ifndef USE_LIBC_UTMP
#define f_endutent() close(fd)
#define f_getutent() utmp_io(fd, ut, F_RDLCK)
#endif

#ifndef HAVE_C_UTMPX
#include <utmp.h>
#define utmp_type	utmp
#define Utmp_File	_PATH_UTMP
#define Wtmp_File	_PATH_WTMP
#endif

#ifdef HAVE_C_UTMP
#define f_setutent	setutent
#define f_endutent	endutent
#define f_getutent()	(ut = getutent())
#define f_pututline	pututline
#define f_updwtmp	updwtmp
#define f_utmpname	utmpname
#endif

#ifdef HAVE_C_UTMPX
#define __USE_GNU
#include <utmpx.h>
#define Utmp_File	_PATH_UTMPX
#define Wtmp_File	_PATH_WTMPX
#define utmp_type	utmpx
#define f_setutent	setutxent
#define f_endutent	endutxent
#define f_getutent()	(ut = getutxent())
#define f_pututline	pututxline
#define f_updwtmp	updwtmpx
#define f_utmpname	utmpxname
#endif

#define UTMP_SIZE sizeof(struct utmp_type)
#endif
